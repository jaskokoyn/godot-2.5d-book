---
description: >-
  Welcome to the official book for the Godot 4 C# Action Adventure: Build your
  own 2.5D RPG Course from GameDev.tv.
---

# Godot 4 C# Action Adventure: Build your own 2.5D RPG

<figure><img src=".gitbook/assets/Godot Book Cover.png" alt=""><figcaption></figcaption></figure>

## What is this book?&#x20;

This book was created to summarize each lecture from the course. Watching videos can be a great way to learn, but they have flaws. Scrubbing through a video can be tedious. As humans, we can read faster than watching a video.&#x20;

For this reason, the book was created to act as a supplementary resource. You can use it to recap what you've learned and review content from previous lectures. Hopefully, this resource will prove to be invaluable to you.&#x20;

## Where to buy the course?

You can find the course here: [https://www.gamedev.tv/p/godot-c-action-adventure?coupon\_code=OFFER](https://www.gamedev.tv/p/godot-c-action-adventure?coupon\_code=OFFER)
