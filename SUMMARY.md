# Table of contents

* [Godot 4 C# Action Adventure: Build your own 2.5D RPG](README.md)
* [Section 1: Introduction and Setup](section-1-introduction-and-setup.md)
* [Section 2: Player Movement](section-2-player-movement.md)
* [Section 3: State Machine](section-3-state-machine.md)
* [Section 4: Designing a Level](section-4-designing-a-level.md)
* [Section 5: Adding Enemies](section-5-adding-enemies.md)
* [Section 6: Combat System](section-6-combat-system.md)
* [Section 7: Game Interface](section-7-game-interface.md)
* [Section 8: Finishing Touches](section-8-finishing-touches.md)
