---
description: >-
  In this section, we get a new project up and running for the rest of this
  course.
---

# Getting Started

## Installing .NET and Godot

In this lecture, we installed the necessary software for running Godot with C#. Check out the resource links for the download links. We performed the following steps:

1. **Install .NET SDK -** Since C# is not a programming language maintained by Godot, it's required to install the .NET SDK.&#x20;
2. **Download Godot** - After installing the SDK, you can proceed to download Godot.  Make sure you're downloading the version with **.NET** support. Otherwise, your C# code will not be compiled.&#x20;
3. **Extract the Zip File -** Be sure to place Godot in a directory separate from your other files to keep things organized.&#x20;

#### Resources

* .NET SDK -  [https://dotnet.microsoft.com/en-us/download](https://dotnet.microsoft.com/en-us/download)
* Godot Engine - [https://godotengine.org/](https://godotengine.org/)

### Starting a New Project

