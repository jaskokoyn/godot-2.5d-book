---
description: >-
  In this section, we get a new project up and running for the rest of this
  course.
---

# Section 1: Getting Started

## Installing .NET and Godot

In this lecture, we installed the necessary software for running Godot with C#. Check out the resource links for the download links. We performed the following steps:

1. **Install .NET SDK -** Since C# is not a programming language maintained by Godot, it's required to install the .NET SDK.&#x20;
2. **Download Godot** - After installing the SDK, you can download Godot.  Make sure you're downloading the version with **.NET** support. Otherwise, your C# code will not be compiled.&#x20;
3. **Extract the Zip File -** Be sure to place Godot in a directory separate from your other files to keep things organized.&#x20;

#### Resources

* .NET SDK -  [https://dotnet.microsoft.com/en-us/download](https://dotnet.microsoft.com/en-us/download)
* Godot Engine - [https://godotengine.org/](https://godotengine.org/)

## Starting a New Project

In this lecture, we started a new project. From the project list window, you can create a new project. The following options can be configured.

* **Project Name -** The name of your project/game.
* **Project Path -** The directory where your project will be created. It's recommended to work inside a directory dedicated to your games for organization.
* **Renderer -** The renderer that will be used for rendering your game's graphics. Generally, **Forward+** is the best option for graphic-intensive games.&#x20;
* **Version Control Metadata -** Godot can generate Git files to help you keep track of your project's file history.

### Changing the Icon

A project's icon can be configured by going to **Project > Project Settings > General > Application > Config**. There will be an option called **Icon** to change the project's icon. PNG and SVG images are supported, but SVG images are recommended because they can be resized and edited with a Vector program, like Adobe Illustrator.&#x20;

Check out the resource section for free SVG icons that you can use in your game. After selecting an icon, you can go back to the project list by navigating to the **Project > Quit to Project List** menu option.

#### Resources

* SVG Repo - [https://www.svgrepo.com/](https://www.svgrepo.com/)



{% file src=".gitbook/assets/Assets.zip" %}
Project Asset Files
{% endfile %}

## Check out the book

👀What ya looking for?&#x20;

#### Resources

* Godot RPG Course Book - [https://luisramirez.gitbook.io/godot-2.5d-rpg-course/](https://luisramirez.gitbook.io/godot-2.5d-rpg-course/)

