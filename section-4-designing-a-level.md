---
description: >-
  In this lecture, we started importing 3D models and laying them out around our
  game while also adding collision detection to bump into walls and walk up
  stairs.
---

# Section 4: Designing a Level

## Section Intro - Designing a Level

There are no notes for this lecture.

## Creating a Mesh Library

In this lecture, we learned how to create a mesh library resource. Godot supports various file formats for 3D formats. The two most popular options for Godot is FBX and GLTF. It's recommended to use GLTF because it's free and open-source, so that's what we're using in this course.

**What is a mesh?**

You'll hear the word "mesh" a lot regarding 3D modeling. A mesh describes a 3D object with vertices, edges, and faces. So, a mesh library is just a file containing a collection of meshes.&#x20;

We used the models from the **Dungeon Pack Remastered** asset from **Kay  Lousberg**. You can find the asset in the resource section of this lecture.&#x20;

### Resources

* Dungeon Pack Remastered - [https://kaylousberg.itch.io/kaykit-dungeon-remastered](https://kaylousberg.itch.io/kaykit-dungeon-remastered)&#x20;
* Available 3D Formats - [https://docs.godotengine.org/en/stable/tutorials/assets\_pipeline/importing\_3d\_scenes/available\_formats.html](https://docs.godotengine.org/en/stable/tutorials/assets\_pipeline/importing\_3d\_scenes/available\_formats.html)

## Using a Grid Map

In this lecture, we used a grid map. First, we must export the scene containing our meshes into a resource. A resource is simply a file containing data. This data can be anything, from player stats to inventory items. Godot allows for a scene to be exported as a mesh library resource, which is a file containing information about the meshes in a scene.

Once a file has been exported, you can use the mesh library with the **GridMap** node from Godot. This node takes care of spacing out 3D models on a grid. It can be customized via the inspector. Watch the lecture to learn how to customize some of the settings.

A few things worth mentioning:

* You can use the **SHIFT + F** keyboard shortcut to move while editing in grid mode.
* You can use the **WASD** keys to rotate a model.

## Attaching a Camera to the Player

In this lecture, we attached a camera to the player so that it follows the player around. There are different ways of performing this process, but the simplest way is to simply make the camera node a child of the player. By doing so, the camera's position becomes relative to the player's position. So, if the player moves, so does the camera.

## Adding Collision

In this lecture, we learned how to add collision to an object. There are two steps, which are to add layers to a node and then define the shape of the node.&#x20;

### What are layers and masks?

Layers determine where an object exists in the game. On top of defining where it exists, we can also configure what objects it can detect, which is called a mask.&#x20;

### What are shapes?

Shapes determine the physical shape of the node. Since models and sprites can come in differents shapes and sizes, we must explicitly tell Godot what the physical form of our game objects are.

By adding the proper layers, masks, and shapes, you can enable collision between objects.&#x20;

While optional, it's recommended to assign names to your layers in the **Project > Project Settings > Layer Names > 3D Physics** section.

<figure><img src=".gitbook/assets/image.png" alt=""><figcaption><p>Layer Names</p></figcaption></figure>

### Applying Physics Shapes

If you're working with a sprite, you can add a collision shape with the **CollisionShape3D** node. However, if you're working with an imported model, you must double-click on the model file directly and configure the import settings. Once you'e added a shape, you can reimport the model. Generally speaking, the two most popular options for shapes are:

* **Simple Convex -** Draws a simple shape around the model.
* **Trimesh** - Draws a shape that accurately represents the mesh.

## Adjusting the Slope Angle

In this lecture, we learned how to adjust the floor settings for the **CharacterBody3D** node. There are two settings we modified.

* **Max Angle** - The maximum angle for a slope that can be walked on.
* **Snap Length -** The strength for if an object can stay attached to a slope. The higher the value, the stronger the connection will be between the object and floor. Low values will allow objects to fly into the air when they pick up speed.

## Adding Invisible Walls

In this lecture, we added invisible walls to prevent the player from walking into areas where they shouldn't be able to walk into. We used a combination of **StaticBody3D** and **CollisionShape3D** nodes to accomplish this behavior.

During this lecture, we talked about an important feature of resources, which is that Godot only loads a resource once. If we duplicate a node with a resource, that resource will be referenced instead of generating a new copy. If you duplicate a node with a resource, you can generate a unique copy by clicking on the **Make Copy** option for a resource.&#x20;
